// https://youtu.be/ivDjWYcKDZI?t=7588
import React from 'react';
import 'materialize-css';
import { BrowserRouter } from 'react-router-dom';

import Loader from './components/Loader';
import NavBar from './components/NavBar';
import AuthContext from './context/AuthContext';
import { useAuth } from './hooks/auth.hook';
import { useRoutes } from './routes';

export default function App() {
  const {
    token, login, logout, userId, ready,
  } = useAuth();
  const isAuthenticated = !!token;
  const routes = useRoutes(isAuthenticated);

  if (!ready) {
    return <Loader />;
  }

  return (
    <AuthContext.Provider
      value={{
        token,
        login,
        logout,
        userId,
        isAuthenticated,
      }}
    >
      <BrowserRouter>
        {isAuthenticated && <NavBar />}
        <div className='container'>
          <div>{routes}</div>
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}
