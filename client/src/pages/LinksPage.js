import React, {
  useState, useContext, useCallback,
  useEffect,
} from 'react';

import LinksList from '../components/LinksList';
import Loader from '../components/Loader';
import AuthContext from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';


export default function LinksPage() {
  const [links, setLinks] = useState([]);
  const { loading, request } = useHttp();
  const { token } = useContext(AuthContext);

  const fetchLinks = useCallback(
    async () => {
      try {

        const result = await request(
          '/api/link',
          'GET',
          null,
          { Authorization: `Bearer ${token}` },
        );

        setLinks(result);

      } catch (error) {
        console.log(error.message);

      }
    },
    [request, token],
  );

  useEffect(() => {
    fetchLinks();
  }, [fetchLinks]);

  return (
    <>
      {
        loading
          ? <Loader />
          : <LinksList links={links} />
      }
    </>
  );
}
