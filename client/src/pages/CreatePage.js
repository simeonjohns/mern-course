import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';

import AuthContext from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export default function CreatePage() {
  const auth = useContext(AuthContext);
  const [link, setLink] = useState('');
  const { request } = useHttp();
  const history = useHistory();

  const handleKeyDown = async e => {
    if (e.key === 'Enter') {
      try {
        const result = await request(
          '/api/link/generate',
          'POST',
          { from: link },
          { Authorization: `Bearer ${auth.token}` },
        );

        history.push(`/detail/${result.link._id}`);

      } catch (error) {
        console.log(error.message);
      }
    }
  };

  const handleChange = e => {
    setLink(e.target.value);
  };


  return (
    <div className='row'>
      <div
        className='col s8 offset-s2'
        style={{ paddingTop: '2rem' }}
      >
        <div className='input-field'>
          <input
            name='link'
            id='link'
            type='text'
            onChange={handleChange}
            value={link}
            onKeyPress={handleKeyDown}
          />
          <label htmlFor='link'>{'Link'}</label>
        </div>
      </div>
    </div>
  );
}
