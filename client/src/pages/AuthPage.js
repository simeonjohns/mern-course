import React, {
  useState, useEffect, useContext,
} from 'react';

import AuthContext from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { useMessage } from '../hooks/message.hook';


export default function AuthPage() {
  const message = useMessage();

  const auth = useContext(AuthContext);

  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const {
    loading,
    error,
    request,
    clearError,
  } = useHttp();

  const handleChange = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    message(error);
    clearError();
  }, [clearError, error, message]);

  const handleRegister = async () => {
    try {
      const data = await request(
        '/api/auth/register',
        'POST',
        { ...form },
      );
      message(data.message);
    } catch (e) {
      console.log(e);
    }
  };

  const handleLogin = async () => {
    try {
      const data = await request(
        '/api/auth/login',
        'POST',
        { ...form },
      );
      auth.login(data.token, data.userId);
      message(data.message);
    } catch (e) {
      console.log(e);
    }
  };


  return (
    <div className='row'>
      <div className='col s6 offset-s3'>
        <h1>{'Reduce URL'}</h1>
        <div className='card blue darken-1'>
          <div className='card-content white-text'>
            <span className='card-title'>{'Authorization'}</span>
            <div>

              <div className='input-field white-color'>
                <input
                  name='email'
                  // placeholder='Enter email'
                  id='email'
                  type='text'
                  className='validate'
                  onChange={handleChange}
                  value={form.email}
                />
                <label htmlFor='email'>{'Email'}</label>
              </div>
              <div className='input-field white-color'>
                <input
                  name='password'
                  // placeholder='Enter password'
                  id='password'
                  type='password'
                  className='validate'
                  onChange={handleChange}
                  value={form.password}
                />
                <label htmlFor='password'>{'Password'}</label>
              </div>

            </div>
          </div>
          <div className='card-action'>
            <button
              onClick={handleLogin}
              disabled={loading}
              type='button'
              className='btn yellow darken-4'
              style={{ marginRight: 10 }}
            >
              {'Sign In'}
            </button>
            <button
              onClick={handleRegister}
              disabled={loading}
              type='button'
              className='btn grey lighten-1 black-text'
            >
              {'Sign Up'}
            </button>
          </div>
        </div>

      </div>
    </div>
  );
}
