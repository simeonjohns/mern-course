import React, {
  useState, useCallback, useContext, useEffect,
} from 'react';
import { useParams } from 'react-router-dom';

import LinkCard from '../components/LinkCard';
import Loader from '../components/Loader';
import AuthContext from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';


export default function DetailsPage() {
  const { token } = useContext(AuthContext);
  const { request, loading } = useHttp();
  const [link, setLink] = useState(null);
  const { id } = useParams();

  const getLink = useCallback(
    async () => {
      try {

        const result = await request(
          `/api/link/${id}`,
          'GET',
          null,
          { Authorization: `Bearer ${token}` },
        );

        setLink(result);

      } catch (error) {
        console.log(error.message);
      }
    },
    [id, request, token],
  );

  useEffect(() => { getLink(); }, [getLink]);

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      {
        !loading && link &&
        <LinkCard link={link} />
      }
    </>
  );
}
