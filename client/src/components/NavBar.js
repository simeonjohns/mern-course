import React, { useContext } from 'react';
import { NavLink, useHistory } from 'react-router-dom';

import AuthContext from '../context/AuthContext';

export default function NavBar() {
  const auth = useContext(AuthContext);
  const history = useHistory();

  const handleLogout = e => {
    e.preventDefault();
    auth.logout();
    history.push('/');
  };

  return (
    <nav>
      <div className='nav-wrapper'>
        <a
          href='/'
          className='brand-logo'
        >
          {'Link shortener'}
        </a>
        <ul
          id='nav-mobile'
          className='right hide-on-med-and-down'
        >
          <li>
            <NavLink
              to='/create'
            >
              {'Create\r'}
            </NavLink>
          </li>
          <li>
            <NavLink
              to='/links'
            >
              {'Links\r'}
            </NavLink>
          </li>
          <li>
            <a
              href='/'
              onClick={handleLogout}
            >
              {'Logout\r'}
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
}
