import React from 'react';
import { useHistory } from 'react-router-dom';

export default function LinksList({ links }) {
  const history = useHistory();

  return (
    links.length
      ? (
        <table className='highlight'>
          <thead>
            <tr>
              <th>{'№'}</th>
              <th>{'Short link'}</th>
              <th>{'Old link'}</th>
              <th>{'Clicks'}</th>
            </tr>
          </thead>

          <tbody>
            {
              links.map((link, index) => (
                <tr
                  key={link._id}
                  style={{ cursor: 'pointer' }}
                  onClick={() => history.push(`/detail/${link._id}`)}
                >
                  <td>{index + 1}</td>
                  <td>{link.from}</td>
                  <td>{link.to}</td>
                  <td>{link.clicks}</td>
                </tr>
              ))
            }

          </tbody>
        </table>
      )
      : <p className='center'>{'There are no links yet'}</p>
  );
}
